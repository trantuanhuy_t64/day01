CREATE DATABASE QLSV
USE QLSV
CREATE TABLE DMKHOA (
    MaKH VARCHAR(6) PRIMARY KEY, 
    TenKhoa VARCHAR (30));
CREATE TABLE SINHVIEN (
    MaSV VARCHAR(6) PRIMARY KEY, 
    HoSV VARCHAR(30), 
    TenSV VARCHAR(15), 
    GioiTinh CHAR(1), 
    NgaySinh DATETIME, 
    NoiSinh VARCHAR(50), 
    DiaChi VARCHAR(50), 
    MaKH VARCHAR(6), 
    HocBong INT)
ALTER TABLE SINHVIEN ADD FOREIGN KEY(MaKH) REFERENCES DMKHOA(MaKH)
INSERT INTO DMKHOA 
VALUES 
    ('CNTT01', 'Công nghệ thông tin'), 
    ('TCTH01', 'Toán Cơ Tin Học') 
INSERT INTO SINHVIEN
VALUES 
    ('190436', 'Trần', 'Tuấn Huy', 'N', '2001-11-16', 'Hà Nội', '303 Phương Mai', 'CNTT01', 1000000), 
    ('190390', 'Nguyễn', 'Kim Hoàng', 'N', '2002-02-22', 'Hà Nội', '360 Phương Mai', 'TCTH01', 2000000)
SELECT * FROM sinhvien WHERE sinhvien.MaKH = (SELECT MaKH FROM DMKHOA WHERE TenKhoa = "Công nghệ thông tin");

